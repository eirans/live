module.exports = function(grunt){
 
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),
        jade: {
            live: {
                options: { client: false, pretty: true, data: { debug: true } },
                files: [
                    { cwd: './template/jade', expand: true, src: '*.jade', dest: './app', ext: '.html' }
                ]
            }
        },
        less: {
            live: {
                options: { compress: false, yuicompress: true, optimization: 2 },
                files: [
                    { cwd: './template/less/', expand: true, src: ['*.less'], dest: './app/css', ext: '.css' },
                ]
            }
        },
        watch: {
            live: {
                files: ['./template/jade/**/*.jade', './template/less/**/*.less'],
                tasks: ['jade:live', 'less:live'],
                options: {
                  browserSync: true,
                }
            }
        },
        browserSync: {
            live: {
                bsFiles: {
                    src : [
                        './app/**/*.css',
                        './app/**/*.jsp',
                        './app/**/*.html',
                        './app/**/*.js'
                    ]
                },
                options: {
                    proxy: "live.ux",
                    watchTask: true
                }
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-jade');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browser-sync');
    //grunt.loadNpmTasks('grunt-contrib-jshint');
    //grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.registerTask('default', ['jade:live', 'less:live', 'browserSync:live', 'watch:live']);

};
